package rizka.shelvia.aplikasigmbatik

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_user.*
import kotlinx.android.synthetic.main.activity_user.view.*




class fragmentuser : Fragment(), View.OnClickListener, AdapterView.OnItemSelectedListener {

    lateinit var thisParent: MainActivity
    lateinit var lsAdapter: SimpleCursorAdapter
    lateinit var spAdapter: SimpleCursorAdapter
    lateinit var dialog: AlertDialog.Builder
    lateinit var v: View
    var idUser : String = ""
    var namaLevel: String = ""
    lateinit var db: SQLiteDatabase

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.activity_user, container, false)
        db = thisParent.getdataobjec()
        dialog = AlertDialog.Builder(thisParent)
        v.insert.setOnClickListener(this)
        v.edite.setOnClickListener(this)
        v.del.setOnClickListener(this)
        v.spinner.onItemSelectedListener = this
        v.lsuser.setOnItemClickListener(clik)
        showDataUser("")
        return v
    }

    override fun onStart() {
        super.onStart()
        showDataLevel()
        showDataUser("")
    }
    fun showDataLevel() {
        val c: Cursor =
            db.rawQuery("select nama_level as _id from level order by nama_level asc", null)
        spAdapter = SimpleCursorAdapter(
            thisParent,
            android.R.layout.simple_spinner_item,
            c,
            arrayOf("_id"),
            intArrayOf(android.R.id.text1),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spinner.adapter = spAdapter
        v.spinner.setSelection(0)
    }

     fun showDataUser(namaUser: String) {
        var sql = ""

        if (!namaUser.trim().equals("")) {
            sql = "select u.id_user as _id, u.nama,u.passwor, l.nama_level from user u, level l" +
                    "where u.id_level=l.id_level and u.nama like '%$namaUser%'"

        } else {

            sql = "select u.id_user as _id, u.nama,u.password, l.nama_level from user u, level l " +
                    "where u.id_level=l.id_level order by u.nama asc"
        }
         val c: Cursor = db.rawQuery(sql, null)
        lsAdapter = SimpleCursorAdapter(
            thisParent,
            R.layout.isi_user,
            c,
            arrayOf("_id", "nama","password","nama_level"),
            intArrayOf(R.id.txid_user, R.id.txusername,R.id.textView8,R.id.txlevel),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
        )
        v.lsuser.adapter = lsAdapter
     }



    override fun onNothingSelected(parent: AdapterView<*>?) {
        spinner.setSelection(0, true)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val c: Cursor = spAdapter.getItem(position) as Cursor
        namaLevel = c.getString(c.getColumnIndex("_id"))

    }


    // clik action
    override fun onClick(v: View?){
        when (v?.id) {
            R.id.insert->{
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang dimasukkan sudah benar?")
                    .setPositiveButton("Ya", btnInsertDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
            R.id.edite->{
                dialog.setTitle("Konfirmasi").setMessage("Data yang dimasukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya", btup)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
            R.id.del -> {
                dialog.setTitle("Konfirmasi").setMessage("Yakin akan menghapus data ini?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya", btndel)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
            R.id.Cari-> {
                showDataUser(edusername.text.toString())
            }
        }
    }

    fun insertDataUser(
        id_user : String,
        namaUser: String,
        password : String,
        id_level : Int

    ) {
        var sql = "insert into user(id_user,nama, password, id_level) values (?,?,?,?)"
        db.execSQL(sql, arrayOf(id_user, namaUser,password, id_level))
    }

    fun update(
        id_user: String,
        namaUser: String,
        id_level: Int,
        password: String
    ){
        var cv : ContentValues = ContentValues()
        cv.put("nama",namaUser)
        cv.put("id_level",id_level)
        cv.put("password",password)
        db.update("user",cv,"id_user= '$id_user'",null)
        showDataUser("")
    }

    fun deletDataUser(idUser :String) {
        db.delete(" user ","id_user = '$idUser'",null)
        showDataUser("")
    }

    val btup=DialogInterface.OnClickListener { dialog, which ->

        var sql = "select id_level from level where nama_level ='$namaLevel'"
        val c: Cursor = db.rawQuery(sql, null)
        if (c.count > 0) {
            c.moveToFirst()
            update(
                v.ediduser.text.toString(), v.edusername.text.toString(),c.getInt(c.getColumnIndex("id_level")), v.edpassword.text.toString()

            )
            v.ediduser.setText("")
            v.edusername.setText("")
            v.edpassword.setText("")

        }
    }

    val btndel = DialogInterface.OnClickListener { dialog, which ->
        deletDataUser(idUser)
        v.edusername.setText("")
        v.ediduser.setText("")
        v.edpassword.setText("")
    }

    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        var sql = "select id_level from level where nama_level ='$namaLevel'"
        val c: Cursor = db.rawQuery(sql, null)
        if (c.count > 0) {
            c.moveToFirst()
            insertDataUser(
                v.ediduser.text.toString(), v.edusername.text.toString(), v.edpassword.text.toString(),
                c.getInt(c.getColumnIndex("id_level"))
            )
            v.ediduser.setText("")
            v.edusername.setText("")
            v.edpassword.setText("")

        }
    }

    val clik = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c: Cursor = parent.adapter.getItem(position) as Cursor
        idUser = c.getString(c.getColumnIndex("_id"))
        v.ediduser.setText(c.getString(c.getColumnIndex("_id")))
        v.edusername.setText(c.getString(c.getColumnIndex("nama")))
        v.edpassword.setText(c.getString(c.getColumnIndex("password")))
        //v.inputnil.setText(c.getString(c.getColumnIndex("nama_jenis")))

    }
}


//fun updateDataMhs(nimMhs: String, namaMhs: String, namaProdi: Int) {
  //  var sql = "update mhs set nama='$namaMhs', id_prodi = '$namaProdi' where  nim='$nimMhs',"
    //db.execSQL(sql, arrayOf(nimMhs, namaMhs, namaProdi))
    //showDataMhs("")
//}
