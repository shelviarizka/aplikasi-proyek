package rizka.shelvia.aplikasigmbatik

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class OpenHelper(context: Context):SQLiteOpenHelper(context,Db_name,null,Db_ver) {

    companion object{
        val Db_name = "mahasiswa"
        val Db_ver = 1
    }


    override fun onCreate(db: SQLiteDatabase?) {


        val tUser = "create table user(id_user text primary key, nama text not null , password text not null, id_level text not null)"

        val  tBarang = "create table Barang(id_barang text primary key , id_kategori text not null, nama_barang text not null, stok Int not null, hargajual Int not null, hargabeli Int not null)"
        val tKategori = "create table kategori(id_kategori integer primary key autoincrement, nama_kategori text not null)"
        val  tlevel = "create table level(id_level integer primary key autoincrement, nama_level text not null)"
        val insKategori = "insert into kategori(nama_kategori) values ('ROK'),('KEMEJA')"
        val inslevel = "insert into level(nama_level) values ('OWNER'),('STAF')"
        //tmk
        //val tmk = "create table mk(kode text primary key , nama_mk text not null, id_prodi Int not null )"
        //val tnil = "create table nilai(id integer primary key autoincrement,nim text not null, kode text not null, nil text not null)"
        //val insprodi = "insert into prodi(nama_prodi) values ('MI'),('AK'),('ME'),('PR')"

        db?.execSQL(tlevel)
        db?.execSQL(tUser)

        db?.execSQL(tBarang)

        db?.execSQL(tKategori)
        db?.execSQL(insKategori)


        db?.execSQL(inslevel)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


}